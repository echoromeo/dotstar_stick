#!/usr/bin/python
# -*- coding: latin-1 -*-
#
# © 2018 Microchip Technology Inc. and its subsidiaries.
#
# Subject to your compliance with these terms, you may use Microchip software
# and any derivatives exclusively with Microchip products. It is your
# responsibility to comply with third party license terms applicable to your
# use of third party software (including open source software) that may
# accompany Microchip software.
#
# THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS".  NO WARRANTIES, WHETHER
# EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY
# IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A
# PARTICULAR PURPOSE. IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT,
# SPECIAL, PUNITIVE, INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR
# EXPENSE OF ANY KIND WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED,
# EVEN IF MICROCHIP HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE
# FORESEEABLE. TO THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL
# LIABILITY ON ALL CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED
# THE AMOUNT OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR
# THIS SOFTWARE.

# Serial bootloader script for Microchip tinyAVR 0-, 1-series and megaAVR 0-series MCUs
#
# Futurized to support python 2 and 3.
# Download Python from https://www.python.org/downloads/ 
#
from __future__ import print_function
try:
	import sys
	from builtins import bytes, int
	import argparse
	from serial import Serial
	import codecs
	import csv    
except ImportError:
    sys.exit("""ImportError: You are probably missing some modules.
To add needed modules, run 'python -m pip install -U future pyserial'""")

# Generate help and use messages 
parser = argparse.ArgumentParser(
description='Serial loading script for Microchip tinyAVR 0-, 1-series and megaAVR 0-series MCUs',
epilog='Example: tiny_uploader.py config.csv 3 COM130 115200',
formatter_class=argparse.RawTextHelpFormatter)
parser.add_argument('file', help='file to upload')
parser.add_argument('pattern', help='Pattern 1-10 to store')
parser.add_argument('comport', help='UART COM port')
parser.add_argument('baudrate', help='UART baud rate')
if len(sys.argv)==1:
    parser.print_help()
    sys.exit(1)
args = parser.parse_args()

# Command line arguments
file = sys.argv[1]
pattern = int(sys.argv[2], 0)
comport = sys.argv[3]
baudrate = int(sys.argv[4], 0)

data = [pattern]
with codecs.open(file, 'r', encoding='utf_8_sig') as infile:
    reader = csv.reader(infile, delimiter=";")
    for r,g,b in reader:
        data += [int(i) for i in [b, g, r]]

# Init serial communication
uart = Serial(comport, baudrate=baudrate, timeout=1)

# Send data to device. Each byte is is echoed for validation.
size = len(data)
failed = 0
counter = 0

print ("Uploading", size, "bytes...")
for byte in data:
    byte = bytes([byte])
    uart.write(byte)
    tmp_byte = uart.read(1)
    if (tmp_byte != byte):
        failed = 1
        break
    counter += 1
    print ("\r%.02f%%" % (float(counter)/size*100), end='')

# Check reason for exiting loop and print debug info
if failed:
    print ("\nFailed at byte %i" % (counter))
    tmp_byte = int.from_bytes(tmp_byte, byteorder="little")
    byte = int.from_bytes(byte, byteorder="little")
    print ("Value %#04x echoed, expected %#04x" % (tmp_byte, byte))
else:
    print ("\nOK")

# Device will reset and jump to application when flash is successfully programmed,
# or it can be externally reset or power-cycled.