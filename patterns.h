#ifndef _PATTERNS_H_
#define _PATTERNS_H_

typedef __uint24 uint24_t;

/* Patterns struct */
typedef struct patterns_struct {
	uint24_t rgb[NUM_LEDS];
	uint8_t reserved[PROGMEM_PAGE_SIZE - ((NUM_LEDS*3)%PROGMEM_PAGE_SIZE)];
} patterns_t;

/* Pointer to patterns */
extern patterns_t *pattern;

/* Interface function prototypes */
void init_uart(void);
void init_status_led(void);
void toggle_status_led(void);

#endif // _PATTERNS_H_