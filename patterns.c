/******************************************************************************
 * © 2018 Microchip Technology Inc. and its subsidiaries.
 *
 * Subject to your compliance with these terms, you may use Microchip software
 * and any derivatives exclusively with Microchip products. It is your
 * responsibility to comply with third party license terms applicable to your
 * use of third party software (including open source software) that may
 * accompany Microchip software.
 *
 * THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS".  NO WARRANTIES, WHETHER
 * EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY
 * IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A
 * PARTICULAR PURPOSE. IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT,
 * SPECIAL, PUNITIVE, INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR
 * EXPENSE OF ANY KIND WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED,
 * EVEN IF MICROCHIP HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE
 * FORESEEABLE. TO THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL
 * LIABILITY ON ALL CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED
 * THE AMOUNT OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR
 * THIS SOFTWARE.
 *
 *****************************************************************************/

/*
 * UART Bootloader for tinyAVR 0- and 1-series, and megaAVR 0-series
 * Each byte received is echoed to confirm reception.
 *
 * For the code to be placed in the constructors section it is necessary
 * to disable standard startup files in Toolchain->AVR/GNU Linker->General.
 *
 * The example is written for ATtiny817 with the following pinout:
 * USART0 TxD PB2
 * USART0 RxD PB3
 * LED0       PB4
 * SW1        PC5 (external pull-up)
 */
#include <avr/io.h>
#include <assert.h>
#include <stdbool.h>
#include <avr/interrupt.h>
#include "config_fuse.h"
#include "main.h"
#include "patterns.h"

/* Baud rate configuration */
#define BOOT_BAUD (115200)

/* Initial patterns when recompiling */
#define NUM_STRONG 9
#define NUM_OFF 1

static const patterns_t patterns_init[10] __attribute__((__used__, __section__(".mypatterns"))) = {
	{.rgb[0 ... NUM_STRONG] = 0x7f7f00, .rgb[NUM_LEDS-1-NUM_STRONG ... NUM_LEDS-1] = 0x007f7f},
	{.rgb[0 ... NUM_STRONG] = 0xff0000, .rgb[NUM_LEDS-1-NUM_STRONG ... NUM_LEDS-1] = 0x00ff00},
	{.rgb[0 ... NUM_STRONG] = 0x00ff00, .rgb[NUM_LEDS-1-NUM_STRONG ... NUM_LEDS-1] = 0x0000ff},
	{.rgb[0 ... NUM_STRONG] = 0x0000ff, .rgb[NUM_LEDS-1-NUM_STRONG ... NUM_LEDS-1] = 0xff0000},
	{.rgb[0 ... NUM_STRONG] = 0xff0000, .rgb[NUM_STRONG+1+NUM_OFF ... NUM_LEDS-NUM_OFF-2-NUM_STRONG] = 0x200000, .rgb[NUM_LEDS-1-NUM_STRONG ... NUM_LEDS-1] = 0xff0000},
	{.rgb[0 ... NUM_STRONG] = 0x00ff00, .rgb[NUM_STRONG+1+NUM_OFF ... NUM_LEDS-NUM_OFF-2-NUM_STRONG] = 0x002000, .rgb[NUM_LEDS-1-NUM_STRONG ... NUM_LEDS-1] = 0x00ff00},
	{.rgb[0 ... NUM_STRONG] = 0x0000ff, .rgb[NUM_STRONG+1+NUM_OFF ... NUM_LEDS-NUM_OFF-2-NUM_STRONG] = 0x000020, .rgb[NUM_LEDS-1-NUM_STRONG ... NUM_LEDS-1] = 0x0000ff},
	{.rgb[0 ... NUM_STRONG] = 0xffff00, .rgb[NUM_STRONG+1+NUM_OFF ... NUM_LEDS-NUM_OFF-2-NUM_STRONG] = 0x202000, .rgb[NUM_LEDS-1-NUM_STRONG ... NUM_LEDS-1] = 0xffff00},
	{.rgb[0 ... NUM_STRONG] = 0x00ffff, .rgb[NUM_STRONG+1+NUM_OFF ... NUM_LEDS-NUM_OFF-2-NUM_STRONG] = 0x002020, .rgb[NUM_LEDS-1-NUM_STRONG ... NUM_LEDS-1] = 0x00ffff},
	{.rgb[0 ... NUM_STRONG] = 0xff00ff, .rgb[NUM_STRONG+1+NUM_OFF ... NUM_LEDS-NUM_OFF-2-NUM_STRONG] = 0x200020, .rgb[NUM_LEDS-1-NUM_STRONG ... NUM_LEDS-1] = 0xff00ff},
};
patterns_t *pattern = (patterns_t *) (FUSECFG_BOOTEND * 0x100);

/* Internal functions */
static uint8_t uart_receive(void);
static void uart_send(uint8_t byte);

/*
 * Communication interface functions
 */
void init_uart(void)
{
	/* Configure UART */
	CDC_USART.CTRLA = USART_RXSIE_bm;
	CDC_USART.CTRLB = USART_RXEN_bm | USART_TXEN_bm | USART_SFDEN_bm;

	/* From datasheet:
	 * Baud rate compensated with factory stored frequency error
	 * Asynchronous communication without Auto-baud (Sync Field)
	 * 20MHz Clock, 3V
	 */
	int32_t baud_reg_val  = (F_CPU * 64) / (BOOT_BAUD * 16);  // ideal BAUD register value
	assert(baud_reg_val >= 0x4A);           // Verify legal min BAUD register value with max neg comp
	int8_t sigrow_val = SIGROW.OSC20ERR3V;  // read signed error
	baud_reg_val *= (1024 + sigrow_val);    // sum resolution + error
	baud_reg_val += 512;                    // compensate for rounding error
	baud_reg_val /= 1024;                   // divide by resolution
	CDC_USART.BAUD = (int16_t) baud_reg_val;   // set adjusted baud rate

	/* Set TxD as output */
	CDC_PORT.DIRSET = CDC_TX_bm;
}

static uint8_t uart_receive(void)
{
	/* Poll for data received */
	while(!(CDC_USART.STATUS & USART_RXCIF_bm));
	return CDC_USART.RXDATAL;
}

static void uart_send(uint8_t byte)
{
	/* Data will be sent when TXDATA is written */
	CDC_USART.TXDATAL = byte;
}

void init_status_led(void)
{
	/* Set LED0as output */
	LED0_VPORT.DIR |= LED0_bm;
	toggle_status_led();
}

void toggle_status_led(void)
{
	/* Toggle LED0 */
	LED0_VPORT.OUT ^= LED0_bm;
}

ISR(USART0_RXC_vect) {
	uint8_t pattern_slot = uart_receive();
	uart_send(pattern_slot);
	
	uint8_t *app_ptr = (uint8_t *) &pattern[pattern_slot-1] + MAPPED_PROGMEM_START;
	uint16_t count = NUM_LEDS*3; // 3 bytes per LED
	while(count--) {
		/* Receive and echo data before loading to memory */
		uint8_t rx_data = uart_receive();
		uart_send(rx_data);

		/* Incremental load to page buffer before writing to Flash */
		*app_ptr = rx_data;
		app_ptr++;
		if(!((uint16_t)app_ptr % MAPPED_PROGMEM_PAGE_SIZE)) {
			/* Page boundary reached, Commit page to Flash */
			_PROTECTED_WRITE_SPM(NVMCTRL.CTRLA, NVMCTRL_CMD_PAGEERASEWRITE_gc);
			while(NVMCTRL.STATUS & NVMCTRL_FBUSY_bm);

			toggle_status_led();
		}
	}
	
	if ((NUM_LEDS*3) % MAPPED_PROGMEM_PAGE_SIZE)
	{
		/* Last page is not full, Commit page to Flash anyways */
		_PROTECTED_WRITE_SPM(NVMCTRL.CTRLA, NVMCTRL_CMD_PAGEERASEWRITE_gc);
		while(NVMCTRL.STATUS & NVMCTRL_FBUSY_bm);
	}
	
	CDC_USART.STATUS = USART_RXSIF_bm;
}