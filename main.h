#ifndef _MAIN_H_
#define _MAIN_H_

// Configure LED hardware setup
//#define NUM_LEDS (140) //1m stick
#define NUM_LEDS (284) //2m stick
//#define DUAL_COLUMN // Two columns of LEDs where the second is offset half the gap between two LEDs

#define CDC_USART			USART0
#define CDC_PORT			PORTB
#define CDC_TX_PINCTRL		PIN2CTRL
#define CDC_TX_bm			PIN2_bm
#define CDC_RX_PINCTRL		PIN3CTRL
#define CDC_RX_bm			PIN3_bm

#define LED0_VPORT			VPORTB
#define LED0_bm				PIN7_bm

// Avoid, used as button 2
// #define SW0_PORT			PORTC
// #define SW0_PINCTRL			PIN4CTRL
// #define SW0_bm				PIN4_bm

// Just a duplicate from dotstar.h
// #define LED_SPI				SPI0
// #define LED_SPI_PORT		PORTC
// #define LED_DATA_PIN        PIN0_bm
// #define LED_SCK_PIN         PIN2_bm

#endif // _MAIN_H_