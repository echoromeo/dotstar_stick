/*
 * dotstar_config.h - copy this to the parent folder of libavr_dotstar_spi and configure
 *
 * Created: 22.03.2020
 * Author: echoromeo
 */
#ifndef DOTSTAR_CONFIG_H_
#define DOTSTAR_CONFIG_H_

/*
 * Needed for the instance defines
 */
#include <avr/io.h>

/*
 * Configure the SPI instance and output pin to be used by the driver
 */
#define LED_SPI				SPI0
#define LED_SPI_PORT		PORTC
#define LED_DATA_PIN        PIN0_bm
#define LED_SCK_PIN         PIN2_bm

/*
 * Define one of these only if you want to move the SPI output to an alternate pinout
 */
#define LED_SPI_PORT_ALT				PORTMUX_SPI0_ALTERNATE_gc //tiny-style
//#define LED_SPI_PORT_ALT1				PORTMUX_SPI0_ALT1_gc //mega-style alt 1
//#define LED_SPI_PORT_ALT2				PORTMUX_SPI0_ALT2_gc //mega-style alt 2

#endif /* DOTSTAR_CONFIG_H_ */
