/*
 * main.c
 *
 * Created: 25.09.2018 19:07:35
 * Author : Egil
 */

/*
 * Include files
 */

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <stdlib.h>
#include <stdbool.h>
#include <util/atomic.h>
#include <assert.h>
#include "config_fuse.h"
#include "libavr_dotstar_spi/dotstar.h"
#include "main.h"
#include "patterns.h"

// Three pins in one ISR/PORT for the buttons
#define BUTTONPORT			PORTC
#define BUTTON1PINCTRL		PIN3CTRL
#define BUTTON1_bm			PIN3_bm
#define BUTTON2PINCTRL		PIN4CTRL
#define BUTTON2_bm			PIN4_bm
#define BUTTON3PINCTRL		PIN5CTRL
#define BUTTON3_bm			PIN5_bm
#define BUTTONPORT_vect		PORTC_PORT_vect

// Five potmeters, B25k in series with a 22k resistor?
#define POT1_PORT			PORTA
#define POT1_PINCTRL		PIN5CTRL
#define	POT1_MUX_gc			ADC_MUXPOS_AIN5_gc
#define POT2_PORT			PORTA
#define POT2_PINCTRL		PIN6CTRL
#define	POT2_MUX_gc			ADC_MUXPOS_AIN6_gc
#define POT3_PORT			PORTA
#define POT3_PINCTRL		PIN7CTRL
#define	POT3_MUX_gc			ADC_MUXPOS_AIN7_gc
#define POT4_PORT			PORTB
#define POT4_PINCTRL		PIN4CTRL
#define	POT4_MUX_gc			ADC_MUXPOS_AIN9_gc
#define POT5_PORT			PORTB
#define POT5_PINCTRL		PIN5CTRL
#define	POT5_MUX_gc			ADC_MUXPOS_AIN8_gc
#define NUM_POTS (5+1)

#define calculate_brightness(x)	(x >> 6) // Max 0x1f 
#define calculate_hue(x) (x % (HSB_MAX_HUE+1))// Max 1536
#define calculate_gap(x) ((x / 16) % NUM_LEDS) // Max NUM_LEDs-1?

/* The values to put in the elf file */
FUSES = {
	.WDTCFG =   FUSECFG_WDTCFG,
	.BODCFG =   FUSECFG_BODCFG,
	.OSCCFG =   FUSECFG_OSCCFG,
	.SYSCFG0 =  FUSECFG_SYSCFG0,
	.SYSCFG1 =  FUSECFG_SYSCFG1,
	.APPEND =   FUSECFG_APPEND,
	.BOOTEND =  FUSECFG_BOOTEND
};

enum modes {
	HUE,
	PATTERN,	
};

enum hue_rotation_modes {
	ROTATE_OFF,
	ROTATE_ACTIVE_LED,
	ROTATE_ACTIVE_SECTION,
	ROTATE_LED,
	ROTATE_UNACTIVE_SECTION,
	NUM_ROTATE_MODES,
};

// Variables that I can't be bothered to put inside main
static uint16_t led_array[NUM_LEDS] = {0};

// Constants
static const uint8_t pot_lut[NUM_POTS] = {POT1_MUX_gc, POT2_MUX_gc, POT3_MUX_gc, POT4_MUX_gc, POT5_MUX_gc, ADC_MUXPOS_INTREF_gc};

// Variables used in interrupts
static volatile enum modes mode = PATTERN;
static volatile enum hue_rotation_modes hue_rotation_mode = ROTATE_OFF;
static volatile uint16_t pot_readings[NUM_POTS] = {0};
static volatile uint8_t battery_low = false;
static volatile uint8_t allow_update = 1;
static volatile uint8_t pattern_num = 0;

// Functions
void dotstar_configure_stick (uint8_t global_brightness);
void poll_pot (void);

/*
 * main()
 */
int main(void)
{
	// Configure clock
#if ((F_CPU == 20000000ul) || (F_CPU == 16000000ul))
	_PROTECTED_WRITE(CLKCTRL.MCLKCTRLB, 0);
#elif ((F_CPU == 10000000ul) || (F_CPU == 8000000ul))
	_PROTECTED_WRITE(CLKCTRL.MCLKCTRLB, CLKCTRL_PDIV_2X_gc | CLKCTRL_PEN_bm);
#endif

	// Configure buttons
	BUTTONPORT.BUTTON1PINCTRL = PORT_PULLUPEN_bm | PORT_INVEN_bm | PORT_ISC_RISING_gc;
	BUTTONPORT.BUTTON2PINCTRL = PORT_PULLUPEN_bm | PORT_INVEN_bm | PORT_ISC_RISING_gc;
	BUTTONPORT.BUTTON3PINCTRL = PORT_PULLUPEN_bm | PORT_INVEN_bm | PORT_ISC_RISING_gc;
	BUTTONPORT.INTFLAGS = BUTTON1_bm;
	BUTTONPORT.INTFLAGS = BUTTON2_bm;
	BUTTONPORT.INTFLAGS = BUTTON3_bm;
	
	// Configure ADC pins
	POT1_PORT.POT1_PINCTRL = PORT_ISC_INPUT_DISABLE_gc;
	POT2_PORT.POT2_PINCTRL = PORT_ISC_INPUT_DISABLE_gc;
	POT3_PORT.POT3_PINCTRL = PORT_ISC_INPUT_DISABLE_gc;
	POT4_PORT.POT4_PINCTRL = PORT_ISC_INPUT_DISABLE_gc;
	POT5_PORT.POT5_PINCTRL = PORT_ISC_INPUT_DISABLE_gc;
	
	// Configure ADC
	VREF.CTRLA = VREF_ADC0REFSEL_2V5_gc;
	ADC0.CTRLB = ADC_SAMPNUM_ACC64_gc;
	ADC0.CTRLC = ADC_SAMPCAP_bm | ADC_PRESC_DIV16_gc | ADC_REFSEL_INTREF_gc;
	ADC0.CTRLD = ADC_INITDLY_DLY64_gc;
	ADC0.SAMPCTRL = 0x1f;
	ADC0.CTRLA = ADC_ENABLE_bm | ADC_RUNSTBY_bm;
	
	// Read initial pot values
	ADC0.MUXPOS = pot_lut[0];
	ADC0.COMMAND = ADC_STCONV_bm;
	for (uint8_t i = 0; i < NUM_POTS; i++)
	{
		poll_pot();
	}
	// Enable ADC interrupt
	ADC0.INTCTRL = ADC_RESRDY_bm;

	init_status_led();
	init_uart();

	// Configure RTC to wake up device after sleep
	while (RTC.STATUS);
	RTC.INTCTRL = RTC_OVF_bm;
	RTC.PER = 32768 / 10; // 10 Hz refresh rate
	RTC.CMP = RTC.PER + 1;
	RTC.CLKSEL = RTC_CLKSEL_INT32K_gc;
	RTC.CTRLA = RTC_RTCEN_bm | RTC_PRESCALER_DIV1_gc | RTC_RUNSTDBY_bm;
	set_sleep_mode(SLEEP_MODE_STANDBY);

	_PROTECTED_WRITE(CPUINT.CTRLA, CPUINT_IVSEL_bm); // Move vector table since we are in boot now
	CPUINT.LVL1VEC = RTC_CNT_vect_num; // Makes it possible to debounce using sleep in interrupt

	sei();

	// Init LEDs to black
	dotstar_init(1);
	sleep_mode();
	
	// Init 300 LEDs to off (dotstar brightness = 0);
	dotstar_configure_constant((color_t) {0}, 400);

	sleep_mode();

	// Enable VLM, 25% above BODLVL
//	BOD.VLMCTRLA = BOD_VLMLVL_25ABOVE_gc;
//	BOD.INTCTRL = BOD_VLMCFG_BELOW_gc | BOD_VLMIE_bm;

	//Initialize loop variables
	uint8_t		refresh = false;
	uint8_t		dotstar_brightness = 1;
	uint8_t		prev_brightness = 1;
	uint32_t	config_hue = 1; // Hue 0 == off
	uint16_t	gap_between_leds = 0;
	uint16_t	leds_between_gaps = 1; // Never completely off
	uint32_t	hue_rotation = 0;
	uint16_t	adc_max = 0;
	while(1) {

		// Go to sleep, wake up on ISR
		sleep_mode();

		if (battery_low)
		{
			refresh = true;

			// Critical battery, turn off all LEDs
			dotstar_brightness = 0; 
			
			// Disable RTC and ADC = Will not wake up again after going to sleep
			RTC.CTRLA = 0;
			ADC0.CTRLA = 0;
		} 
		else if ((pot_readings[5]) && (allow_update))
		{
			refresh = true;

			// Button1 - ?
			// Button2 - Hue rotation mode
			// Button3 - Allow update
			
			// 2048*2048*Rpot/(Rdiv+Rpot)/vdd
			adc_max = 2162012/pot_readings[5];

			// Convert all adc readings to a value relative to vdd
			for (uint8_t i = 0; i < NUM_POTS-1; i++)
			{
				pot_readings[i] =  ((uint32_t) pot_readings[i] * adc_max) / 2048;
			}
			pot_readings[5] = 0;
		
			// Pot1 - Brightness
			dotstar_brightness = ((uint32_t) pot_readings[0] * 0x20) / adc_max;
			if (dotstar_brightness >= 0x20)
			{
				dotstar_brightness = 0x1f;
			}
			
			if ((!prev_brightness) && (!dotstar_brightness))
			{
				refresh = false;
			}
			prev_brightness = dotstar_brightness;
				
			// Pot2 - Hue
			config_hue = ((uint32_t) pot_readings[1] * HSB_MAX_HUE) / adc_max + 1;
			// Pot3 - Hue rotation
			hue_rotation = ((uint32_t)pot_readings[2] * HSB_MAX_HUE) / adc_max;
			if(!hue_rotation) {
				hue_rotation++;
			}
			// Pot4 - Gap between LEDs
			gap_between_leds = ((uint32_t) pot_readings[3] * (NUM_LEDS-1)) / adc_max;
			// Pot5 - LEDs between gaps
			leds_between_gaps = ((uint32_t)pot_readings[4] * (NUM_LEDS-1)) / adc_max;
			if(!leds_between_gaps) {
				leds_between_gaps++;
			}
		
			if (!dotstar_brightness) {
				uint16_t i = NUM_LEDS-1;
				while(i--) {
					led_array[i] = 0;
				}
			}
			else if (mode == HUE)
			{
				uint16_t i = 0;
				uint16_t rotating_hue = config_hue; // Always start with initial hue on LED0
				while(i < NUM_LEDS)
				{
					// Active LEDs
					for (uint16_t led = 0; led < leds_between_gaps; led++)
					{
						if (i < NUM_LEDS)
						{
							led_array[i++] = rotating_hue;
	
							// One hue rotation per active LED
							if (hue_rotation_mode & ROTATE_ACTIVE_LED) // Both ROTATE_ACTIVE_LED and ROTATE_LED
							{
								rotating_hue += hue_rotation;
								if (rotating_hue >= (HSB_MAX_HUE + 1))
								{
									rotating_hue -= HSB_MAX_HUE;
								}
							}
						}
					}
	
					// Single hue rotation per section of active LEDs
					if (hue_rotation_mode == ROTATE_ACTIVE_SECTION)
					{
						rotating_hue += hue_rotation;
						if (rotating_hue >= (HSB_MAX_HUE + 1))
						{
							rotating_hue -= HSB_MAX_HUE;
						}
					}

			
					// Unactive LEDs
					if (gap_between_leds)
					{
						for (uint16_t led = 0; led < gap_between_leds; led++)
						{
							if (i < NUM_LEDS)
							{
								led_array[i++] = 0;
							}
	
							// One hue rotation per unactive LED
							if (hue_rotation_mode == ROTATE_LED)
							{
								rotating_hue += hue_rotation;
								if (rotating_hue >= (HSB_MAX_HUE + 1))
								{
									rotating_hue -= HSB_MAX_HUE;
								}
							}
						}
				
						// Single hue rotation per section of unactive LEDs
						if (hue_rotation_mode == ROTATE_UNACTIVE_SECTION) 
						{
							rotating_hue += hue_rotation;
							if (rotating_hue >= (HSB_MAX_HUE + 1))
							{
								rotating_hue -= HSB_MAX_HUE;
							}
						}
					}
			
					// Clear last section of active LEDs if too small
					if (gap_between_leds)
					{
						if(NUM_LEDS - i <= leds_between_gaps) {
							while (i < NUM_LEDS)
							{
								led_array[i++] = 0;
							}
						}
					}
				}
			}
		}
		
		// Update LEDs
		if (refresh) {
			refresh = false;
			
			if ((mode == HUE) || (!dotstar_brightness))
			{
				dotstar_configure_stick(dotstar_brightness);
			}
			else if (mode == PATTERN)
			{
				dotstar_configure_neopixel_rgb_array(pattern[pattern_num].rgb + MAPPED_PROGMEM_START, NUM_LEDS, dotstar_brightness);
			}
		}		
	}
}

void dotstar_configure_stick(uint8_t brightness)
{
	dotstar_write_start();

#if defined(DUAL_COLUMN)	
	// First column, every second index
	for (uint16_t i = 0; i < (NUM_LEDS/2); i++)
	{
		color_t next_led = {.ones=0x7};
		if (led_array[i*2]) {
			next_led = hsb2rgb((hsb_t) {.h=led_array[i*2]-1, .s=0xff, .b=0xff}, brightness);
		}
		dotstar_write_single(next_led);
	}
	
	// Second (offset) column, every second index from top
	for (uint16_t i = 0; i < (NUM_LEDS/2); i++)
	{
		color_t next_led = {.ones=0x7};
		if (led_array[NUM_LEDS-1 - (i*2)]) {
			next_led = hsb2rgb((hsb_t) {.h=led_array[NUM_LEDS-1 - (i*2)]-1, .s=0xff, .b=0xff}, brightness);
		}
		dotstar_write_single(next_led);
	}

#else
	// Only one column
	for (uint16_t i = 0; i < NUM_LEDS; i++)
	{
		color_t next_led = {.ones=0x7};
		if (led_array[i]) {
			next_led = hsb2rgb((hsb_t) {.h=led_array[i]-1, .s=0xff, .b=0xff}, brightness);
		}
		dotstar_write_single(next_led);
	}
	
#endif
	dotstar_write_end(NUM_LEDS);
}

void poll_pot (void) {
	static volatile uint8_t	pot_index = 0;
	
	// Make sure conversion is complete
	while (!(ADC0.INTFLAGS & ADC_RESRDY_bm)) {}
	// Reading RES will clear flag
	// 64 samples / 32 = 10+1 bit resolution
	pot_readings[pot_index++] = ADC0.RES >> 5; 
	// Last "pot" is VDD
	if (pot_index == NUM_POTS-1)
	{
		ADC0.CTRLC = ADC_SAMPCAP_bm | ADC_PRESC_DIV16_gc | ADC_REFSEL_VDDREF_gc;
	}
	// Wrap around
	else if (pot_index >= NUM_POTS) {
		pot_index = 0;		
		ADC0.CTRLC = ADC_SAMPCAP_bm | ADC_PRESC_DIV16_gc | ADC_REFSEL_INTREF_gc;
	}

	// Swap input and start next conversion
	ADC0.MUXPOS = pot_lut[pot_index];
	ADC0.COMMAND = ADC_STCONV_bm;
}

ISR(RTC_CNT_vect) // LVL1
{
	RTC.INTFLAGS = RTC_OVF_bm;
}

ISR(ADC0_RESRDY_vect) {
	poll_pot();	
}

ISR(BUTTONPORT_vect) {
	// Debounce using LVL1 interrupt, 200~300ms
	sleep_mode();
	sleep_mode();
	sleep_mode();
	
	if (BUTTONPORT.INTFLAGS & BUTTON1_bm)
	{
		if (mode != PATTERN)
		{
			mode = PATTERN;
		} else {
			pattern_num++;
			pattern_num %= 10;		
		}
		BUTTONPORT.INTFLAGS = BUTTON1_bm;
	}
	
	if (BUTTONPORT.INTFLAGS & BUTTON2_bm)
	{
		if (mode != HUE)
		{
			mode = HUE;
		} else {
			hue_rotation_mode++;
			hue_rotation_mode %= NUM_ROTATE_MODES;
		}
		BUTTONPORT.INTFLAGS = BUTTON2_bm;
	}
	
	if (BUTTONPORT.INTFLAGS & BUTTON3_bm)
	{
		allow_update ^= 1;
		if (allow_update)
		{
			LED0_VPORT.OUT |= LED0_bm;
		} else 
		{
			LED0_VPORT.OUT &= ~LED0_bm;
		}
		BUTTONPORT.INTFLAGS = BUTTON3_bm;
	}
}

ISR(BOD_VLM_vect) {
	// Critical battery, turn off all LEDs
	battery_low = true;
	
	BOD.INTFLAGS = BOD_VLMIF_bm;
}

